# Change Log

### 1.1

2020-12-30

- Workaround latest 'fix' for GROOVY-6453 in Groovy 3.0.7 by using early
executing Java code in a fake AST transformation to reset jline system
property

### 1.0

2017-04-07

- Initial version
