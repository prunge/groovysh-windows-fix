package au.net.causal.groovyshwindowsfix;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.control.CompilePhase;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;

/**
 * Not really any kind of code transformer, this class attempts to fix up the stupid
 * jline.terminal 'fix' by later versions of the groovysh batch file on Windows that forces
 * jline.terminal property to none.  Older versions of Groovy didn't do these shenanigans which
 * allowed a preinit.bat in the user's home directory to override jline.terminal property with the
 * fix and that was it.  Unfortunately the later versions of Groovy force this property in their
 * own batch file, so we need some Java code that runs after the Groovy shell starts up but before the
 * terminal is initialized that resets the property to what we want.  Groovy AST transformation hooks
 * run at the perfect time, after Groovy is started but before any terminal / JLine code is initialized.
 * <p>
 *
 * Other options were to use a Java agent or something but this is Groovy specific so it works well.
 * The transform makes no changes so it shouldn't harm anything.  Groovy extension modules were another
 * option but unfortunately those classes weren't actually initialized at startup so our code wasn't run
 * in time.
 * <p>
 *
 * So in summary, the https://github.com/apache/groovy/pull/1413 fix for
 * https://issues.apache.org/jira/browse/GROOVY-6453 really did a number on
 * us and we have to hack our way around it.
 */
@GroovyASTTransformation(phase = CompilePhase.INITIALIZATION)
public class FixUpPropertiesFakeTransformation implements ASTTransformation
{
    static
    {
        //Debugging showed that these transform classes are constructed multiple times, so use
        //a static initializer to ensure this code is only run once
        System.setProperty("jline.terminal", FixedWindowsTerminal.class.getName());
    }

    @Override
    public void visit(ASTNode[] astNodes, SourceUnit sourceUnit)
    {
        //Do nothing
    }
}
