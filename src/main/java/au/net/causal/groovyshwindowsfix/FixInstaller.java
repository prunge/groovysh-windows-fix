package au.net.causal.groovyshwindowsfix;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class FixInstaller 
{
    public static void main(String... args)
    throws IOException
    {
        FixInstaller installer = new FixInstaller();
        if (args.length >= 1)
        {
            if ("install".equals(args[0]))
                installer.install();
            else if ("uninstall".equals(args[0]))
                installer.uninstall();
            else
            {
                System.err.println("Invalid argument " + Arrays.toString(args));
                printUsage();
                System.exit(2);
            }
        }
        else 
        {
            printUsage();
            System.exit(1);
        }
    }
    
    private static void printUsage()
    {
        System.err.println("FixInstaller [install | uninstall]");
    }
    
    private Path groovyDirectory()
    {
        Path userHome = Paths.get(System.getProperty("user.home"));
        return userHome.resolve(".groovy");
    }
    
    public void install()
    throws IOException
    {
        System.out.println("Installing Groovy Windows console fix...");
        Path myJar = thisJarFile();
        Path groovyDir = groovyDirectory();
        Path groovyLibDir = groovyDir.resolve("lib");
        
        Path targetJarFile = groovyLibDir.resolve(myJar.getFileName());
     
        //Copy JAR file to ~/.groovy/lib
        System.out.println("Copying " + myJar + " to " + targetJarFile);
        Files.createDirectories(groovyLibDir);
        Files.copy(myJar, targetJarFile, StandardCopyOption.REPLACE_EXISTING);
        
        //Copy rc script to ~/.groovy
        String preInit = readResource("preinit.bat");
        Path targetPreInitBatFile = groovyDir.resolve("preinit.bat");
        if (Files.exists(targetPreInitBatFile))
        {
            if (!preInit.equals(readFile(targetPreInitBatFile))) 
            {
                throw new RuntimeException("Found existing " + targetPreInitBatFile + " but was changed." + 
                "  Add the following line manually to this file:" + System.lineSeparator() + preInit + System.lineSeparator());
            } 
            else
                System.out.println(targetPreInitBatFile + " already exists, no need to copy");
        }
        else
        {
            System.out.println("Creating " + targetPreInitBatFile);
            try (BufferedWriter w = Files.newBufferedWriter(targetPreInitBatFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW))
            {
                w.write(preInit);
            }
        }
        
        System.out.println("Install completed.");
    }
    
    private String readFile(Path file)
    throws IOException
    {
        return new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
    }
    
    private String readResource(String resourceName)
    throws IOException
    {
        URL resourceUrl = FixInstaller.class.getResource(resourceName);
        try (StringWriter w = new StringWriter(); 
             BufferedReader r = new BufferedReader(new InputStreamReader(resourceUrl.openStream(), StandardCharsets.UTF_8)))
        {
            char[] buf = new char[4096];
            int n;
            do 
            {
                n = r.read(buf);
                if (n > 0)
                    w.write(buf, 0, n);
            }
            while (n >= 0);
            
            return w.toString();
        }
    }
    
    public void uninstall()
    throws IOException
    {
        System.out.println("Uninstalling Windows Groovy console fix");
        Path groovyDir = groovyDirectory();
        
        //Remove preinit.bat if it is exactly the same
        String preInit = readResource("preinit.bat");
        Path targetPreInitBatFile = groovyDir.resolve("preinit.bat");
        if (Files.exists(targetPreInitBatFile))
        {
            if (preInit.equals(readFile(targetPreInitBatFile)))
            {
                System.out.println("Deleting " + targetPreInitBatFile);
                Files.delete(targetPreInitBatFile);
            }
            else
                System.out.println(targetPreInitBatFile + " is different than expected, not deleting.  Remove changes manually if required.");
        }
        else
            System.out.println(targetPreInitBatFile + " does not exist");
        
        //Remove JAR file
        Path groovyLibDir = groovyDir.resolve("lib");
        if (Files.exists(groovyLibDir))
        {
            //Search for our JAR file
            Path thisJar = thisJarFile();
            Path expectedTargetJar = groovyLibDir.resolve(thisJar.getFileName());
            boolean removed = Files.deleteIfExists(expectedTargetJar);
            if (removed)
                System.out.println("Deleted " + expectedTargetJar);
            else
                System.out.println(expectedTargetJar + " does not exist");
        }
    }

    /**
     * Finds the JAR file of this code.
     * 
     * @return a JAR file.
     * 
     * @throws NoSuchFileException if the JAR file of the code could not be determined.
     * @throws IOException if an I/O error occurs.
     */
    private Path thisJarFile()
    throws IOException
    {
        URL url = FixInstaller.class.getResource(FixInstaller.class.getSimpleName() + ".class");
        if (url == null)
            throw new NoSuchFileException("could not find code's JAR.");
        
        try 
        {
            URI uri = url.toURI();
            if (!"jar".equals(uri.getScheme()))
                throw new NoSuchFileException("could not find code's JAR.");
            
            //Trim after/including '!'
            String uriOfJar = uri.getSchemeSpecificPart();;
            int lastExIndex = uriOfJar.lastIndexOf('!');
            
            if (lastExIndex >= 0)
                uriOfJar = uriOfJar.substring(0, lastExIndex);

            return Paths.get(URI.create(uriOfJar));
        }
        catch (URISyntaxException e)
        {
            throw new IOException(e);
        }
    }
}
