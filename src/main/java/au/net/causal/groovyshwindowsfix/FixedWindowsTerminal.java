package au.net.causal.groovyshwindowsfix;

import jline.WindowsTerminal;

import java.io.IOException;
import java.io.InputStream;

public class FixedWindowsTerminal extends WindowsTerminal 
{
    public FixedWindowsTerminal()
    throws Exception
    {
    }

    @Override
    protected boolean isSystemIn(InputStream in) 
    throws IOException 
    {
        //This is the hack
        return true;
    }
}
