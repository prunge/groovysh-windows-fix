# Groovy Shell Windows Fix

Works around the problem in Groovy Shell for Windows where arrow keys, cursor movement
and tab auto-completion does not work.

## Automatic installation

```
mvn dependency:get -Dartifact=au.net.causal.groovysh-windows-fix:groovysh-windows-fix:1.1
java -jar %USERPROFILE%\.m2\repository\au\net\causal\groovysh-windows-fix\groovysh-windows-fix\1.1\groovysh-windows-fix-1.1.jar install
```

Files will be installed in the appropriate places.  When you next run Groovy Shell, the 
fix will be used.

## Manual installation

(use if not comfortable with automatic installation or you already have Groovy customizations)

Download the JAR file from Maven Central, then copy it to the `.groovy\lib` folder
in your user directory.  Then add a `preinit.bat` file in `.groovy` to perform the override
with this:

```
set JAVA_OPTS=%JAVA_OPTS% -Djline.terminal=au.net.causal.groovyshwindowsfix.FixedWindowsTerminal
```

## How it works

This workaround does not modify your Groovy installation, rather it installs a couple of files
in your `.groovy` directory:

- `lib\groovysh-windows-fix-1.1.jar`
- `preinit.bat`

The workaround adds an additional JLine terminal implementation that fixes a problem
with terminal detection.  `preinit.bat`, which is executed before Groovy runs,
overrides the default JLine terminal to the fixed one.
 
## Uninstallation

Run:

```
java -jar %USERPROFILE%\.m2\repository\au\net\causal\groovysh-windows-fix\groovysh-windows-fix\1.1\groovysh-windows-fix-1.1.jar uninstall
```

or manually remove the files the installer added from `.groovy`.
